import os
from setuptools import find_packages, setup


setup(
    name='wmf_mjolnir',
    packages=find_packages(),
    version='2.4.0.dev',
    author='Wikimedia Search Team',
    author_email='discovery@lists.wikimedia.org',
    description='A plumbing library for Machine Learned Ranking at Wikimedia',
    license='MIT',
    entry_points={
        'console_scripts': [
            'mjolnir-utilities.py = mjolnir.__main__:main',
        ],
    },
)
