# MjoLniR - Machine Learned Ranking for Wikimedia

MjoLniR is a library for handling the backend data processing for Machine
Learned Ranking at Wikimedia. It is specialized to how click logs are stored at
Wikimedia and provides functionality to transform the source click logs into ML
models for ranking in elasticsearch.

## Requirements

See conda-environment.yaml.

## Running tests

Test's are run through tox using conda for environment management. You will need
conda to run them.

```
python -m tox
```

## Deployment

### python

Releases and versioning of the python side are managed by the data-engineering
`conda_artifact_repo.yml` ci workflow. See the [workflow documentation](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide/Python_Job_Repos)
for how to make a release

#### tl/dr
Release the current version, bump the version number, and build a new
deployable conda environment for the released version:

1. Browse to [CI Pipelines](https://gitlab.wikimedia.org/repos/search-platform/mjolnir/-/pipelines?page=1&scope=branches&ref=main).
2. The most recent merge automatically triggered the testing jobs against the
   main branch and will be visible here.
3. On the right side of the run click the play (`▶`) dropdown and select
   `trigger_release`. This won't be available until the tests have completed.
4. On completion of `trigger_release` a new git tag will have been created.
   This new tag will trigger the rest of the release process, visible from the
   gitlab pipelines page with a name similar to `Release version 2.0.0`.
5. Once the release has completed an artifact will be available in the project
   [package registry](https://gitlab.wikimedia.org/repos/search-platform/mjolnir/-/packages).
   This artifact is now available for use in airflow or daemon deployments.

### jvm

Releases and versioning of the jvm package are done manually via maven-release-plugin.

## Other

Documentation follows the numpy documentation guidelines:
    https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt
