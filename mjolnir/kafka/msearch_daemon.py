"""
Daemon for collecting elasticsearch queries from a kafka topic, running them,
and pushing the results into a second kafka topic. This runs on the production
side of the network to have access to relforge servers.
"""

from __future__ import annotations
import json
import logging
import multiprocessing.dummy
from queue import Queue
import time
from typing import Callable, Dict, Generator, Iterable, List, Mapping, Optional, TypeVar

import kafka
import kafka.common
import kafka.consumer.subscription_state
import prometheus_client
import requests

import mjolnir.cirrus
import mjolnir.kafka


log = logging.getLogger(__name__)
T = TypeVar('T')
Clock = Callable[[], float]


class Metric(object):
    """A namespace for our runtime metrics"""
    RECORDS_PROCESSED = prometheus_client.Counter(
        'mjolnir_msearch_records_total',
        'Number of kafka records processed')
    PROCESS_BATCH = prometheus_client.Summary(
        'mjolnir_msearch_process_batch_seconds',
        'Time taken to process a batch of records from kafka')


def iter_queue(queue: Queue[Optional[T]]) -> Generator[T, None, None]:
    """Yield items from a queue"""
    while True:
        record = queue.get()
        try:
            # Queue is finished, nothing more will arrive
            if record is None:
                return
            yield record
        finally:
            queue.task_done()


class Daemon(object):
    def __init__(
        self,
        brokers: List[str],
        n_workers: int = 5,
        topic_work: str = mjolnir.kafka.TOPIC_REQUEST,
        topic_result: str = mjolnir.kafka.TOPIC_RESULT,
        topic_complete: str = mjolnir.kafka.TOPIC_COMPLETE,
        max_request_size: int = 4*1024*1024,
        prometheus_port: int = 9161,
        group_id: str = 'mjolnir_msearch',
        max_concurrent_searches: int = 1,
        endpoint: str = 'http://localhost:9200',
    ) -> None:
        """Initialize the msearch daemon

        Parameters
        ----------
        brokers : list of str
            Brokers to use to bootstrap kafka cluster access
        n_workers : int
            The number of requests to issue to elasticsearch in parallel
        topic_work : str
            Kafka topic to read msearch requests from
        topic_result : str
            Kafka topic to write msearch results to
        topic_complete : str
            Kafka topic for sending control messages
        max_request_size : int
            The maximum number of bytes to send in a single kafka produce request
        prometheus_port : int
            Port to expose prometheus metrics collection at
        group_id : str
            Kafka consumer group to join
        max_concurrent_searches : int
            The maximum number of search requests within a single msearch to run in parallel. The
            total concurrent queries possible to issue across a deployment will be:
                    max_concurrent_searches * n_workers * n_kafka_partitions * n_elastic_shards
        endpoint : str
            Elasticsearch endpoint to perform requests against.
        """
        self.brokers = brokers
        self.n_workers = n_workers
        self.topic_work = topic_work
        self.topic_result = topic_result
        self.topic_complete = topic_complete
        self.prometheus_port = prometheus_port
        self.group_id = group_id
        self.max_concurrent_searches = max_concurrent_searches
        # Standard producer for query results
        self.producer = kafka.KafkaProducer(bootstrap_servers=brokers,
                                            max_request_size=max_request_size,
                                            compression_type='gzip',
                                            api_version=mjolnir.kafka.BROKER_VERSION)
        # More reliable producer for reflecting end run sigils. As this
        # is only used for sigils and not large messages like es responses
        # compression is unnecessary here.
        self.ack_all_producer = kafka.KafkaProducer(bootstrap_servers=brokers,
                                                    acks='all',
                                                    api_version=mjolnir.kafka.BROKER_VERSION)
        # No clue how many is appropriate. n_workers seems reasonable enough.
        # We want enough to keep the workers busy, but not so many that the
        # commited offsets are siginficantly ahead of the work actually being
        # performed.
        self.work_queue: Queue[Optional[Mapping]] = Queue(n_workers)
        self.endpoint = endpoint

    def run(self) -> None:
        prometheus_client.start_http_server(self.prometheus_port)
        try:
            self.consume(self.iter_records())
        finally:
            self.producer.close()
            self.ack_all_producer.close()

    def iter_records(self) -> Generator[Mapping, None, None]:
        consumer = kafka.KafkaConsumer(bootstrap_servers=self.brokers,
                                       group_id='mjolnir_msearch',
                                       enable_auto_commit=False,
                                       auto_offset_reset='latest',
                                       value_deserializer=lambda x: json.loads(x.decode('utf8')),
                                       api_version=mjolnir.kafka.BROKER_VERSION,
                                       # Msearch requests are relatively heavy at a few tens of ms each.
                                       # 50 requests at 50ms each gives us ~2.5s to process a batch. We
                                       # keep this low so kafka regularly gets re-pinged.
                                       max_poll_records=min(500, 50 * self.n_workers))
        consumer.subscribe([self.topic_work])
        try:
            last_commit = 0.0
            offset_commit_interval_sec = 60
            offsets: Dict[kafka.TopicPartition, kafka.OffsetAndMetadata] = dict()
            while True:
                now = time.monotonic()
                if offsets and now - last_commit > offset_commit_interval_sec:
                    consumer.commit_async(offsets)
                    last_commit = now
                    offsets = {}
                # By polling directly, rather than using the iter based api, we
                # have the opportunity to regularly re-check the load monitor
                # and transition out of the consuming state if needed.
                poll_response = consumer.poll(timeout_ms=60000)
                if not poll_response:
                    continue
                with Metric.PROCESS_BATCH.time():
                    for tp, records in poll_response.items():
                        for record in records:
                            yield record.value
                    # Wait for all the work to complete
                    self.work_queue.join()
                for tp, records in poll_response.items():
                    offsets[tp] = kafka.OffsetAndMetadata(records[-1].offset + 1, '')
                Metric.RECORDS_PROCESSED.inc(sum(len(x) for x in poll_response.values()))
        finally:
            if offsets:
                consumer.commit(offsets)
            consumer.close()

    def consume(self, records: Iterable[Mapping]) -> None:
        def work_fn() -> None:
            self._handle_records(iter_queue(self.work_queue))

        # Use the dummy pool since these workers will primarily wait on elasticsearch
        worker_pool = multiprocessing.dummy.Pool(self.n_workers, work_fn)
        try:
            for record in records:
                if 'complete' in record:
                    # This is handled directly, rather than queued, because the
                    # consumer guarantees the offset won't be commited until the
                    # next record is consumed. By not consuming any more records
                    # we guarantee at least once processing of these sigils.
                    self._reflect_end_run(record)
                else:
                    self.work_queue.put(record)
        except KeyboardInterrupt:
            # Simply exit the work loop, let everything clean up as expected.
            pass
        finally:
            worker_pool.close()
            for i in range(self.n_workers):
                self.work_queue.put(None)
            worker_pool.join()

        # It is possible, if some workers have errors, for the queue to not be
        # completely emptied. Make sure it gets finished
        if self.work_queue.qsize() > 0:
            log.warning('Work queue not completely drained on shut down. Draining')
            # We call repeatedly because the None values exit the iterator
            while self.work_queue.qsize() > 0:
                work_fn()

    def _reflect_end_run(self, record: Mapping) -> None:
        """Reflect and end run sigil into the complete topic

        This is handled directly in the consumer thread, rather than as part of the
        work queue, to ensure that the offset is not committed to kafka until after
        processing is completed and it has been sucessfully reflected.

        Parameters
        ----------
        record : dict
           Deserialized end run sigil
        """
        log.info('reflecting end sigil for run %s and partition %d' %
                 (record['run_id'], record['partition']))
        # Wait for everything to at least start processing. We don't
        # actually know when the workers are finally idle.
        self.work_queue.join()
        future = self.ack_all_producer.send(
            self.topic_complete, json.dumps(record).encode('utf8'))
        future.add_errback(lambda e: log.critical(
            'Failed to send the "end run" message: %s', e))
        # Wait for ack (or failure to ack)
        future.get()

    def _handle_records(self, work: Iterable[Mapping]) -> None:
        """Handle a single kafka record from request topic

        Parameters
        ----------
        work : iterable of dict
            Yields deserialized requests from kafka to process
        """
        # Standard execution of elasticsearch bulk query
        session = requests.Session()
        for record in work:
            try:
                response = mjolnir.cirrus.make_request(
                    'msearch', session, [self.endpoint], record['request'], reuse_url=True,
                    query_string={'max_concurrent_searches': self.max_concurrent_searches})
                future = self.producer.send(self.topic_result, json.dumps({
                    'run_id': record['run_id'],
                    'meta': record['meta'],
                    'status_code': response.status_code,
                    'text': response.text,
                }).encode('utf8'))
                future.add_errback(lambda e: log.error(
                    'Failed to send a message to the broker: %s', e))
            except:  # noqa: E722
                log.exception('Exception processing record')
